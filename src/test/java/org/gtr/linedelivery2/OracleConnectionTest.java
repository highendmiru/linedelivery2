package org.gtr.linedelivery2;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class OracleConnectionTest {
	
	private static final String DRIVER =
			"oracle.jdbc.driver.OracleDriver";
	private static final String URL =
			"jdbc:oracle:thin:@10.36.60.17:1521:GTRJIS";
	private static final String USER =
			"TRCC";
	private static final String PW =
			"tr2012";
	
	@Test
	public void testConnection() throws Exception{
		Class.forName(DRIVER);
		
		try(Connection con = DriverManager.getConnection(URL, USER, PW)){
			System.out.println(con);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
