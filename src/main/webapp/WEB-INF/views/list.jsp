<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false"%>
<html>
<head>
	<title>List</title>
	<style type="text/css">
		.highlight {
    		background-color: #fff34d;
    		-moz-border-radius: 5px; /* FF1+ */
    		-webkit-border-radius: 5px; /* Saf3-4 */
    		border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
    		-moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
    		-webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
    		box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
		}

		.highlight {
    		padding:1px 4px;
    		margin:0 -4px;
		}
		table {
			width: 200px;
			border-top: 1px solid #444444;
			border-collapse: collapse;
		}
		th, td {
			border-bottom: 1px solid #444444;
			padding: 1px;
			text-align: center;
			font-size: 11px;
		}
	</style>
	<script>
	window.onload = function(){
		 document.getElementById("keywordInput2").focus();	
	}
	</script>
</head>

<body>
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->


		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<a href="scan">Scan</a>&nbsp;&nbsp;&nbsp;<a href="history">History</a>
			</div>
			<div class='box'>
				<div class="box-header with-border">
					<h3 class="box-title">Line Delivery</h3>
				</div>
					<form role="form" action="modifyPage" method="post" id='frm'>
				<div class='box-body'>
					<input type="hidden" name="searchType2" id="searchType2" value="bat">
					<input type="text" name="bat" id="keywordInput2" onkeydown="confirmTrolly()">
				</div>
					</form>
				<input type='hidden' name='searchType' id='searchTypeInput' value="${request.getParameter('searchType')}">
				<input type='hidden' name='keyword' value="${request.getParameter('keyword')}">
			</div>
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered">
						<tr>
							<th>BAT</th>
							<th>JOB</th>
						</tr>
						<c:forEach items="${list}" var="LineDVO" varStatus="status">
							<tr>
								<td><input type="hidden" id="${status.count}" value="${LineDVO.bat}">${LineDVO.bat}</td>
								<td>${LineDVO.job}</td>  								
							</tr>
						</c:forEach>

					</table>
				</div>
				<!-- /.box-body -->

			</div>
		</div>
		<!--/.col (left) -->
		<p></p>
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
	var result = '${msg}';

	if (result == 'SUCCESS') {
		alert("처리가 완료되었습니다.");
	}
	
	function confirmTrolly() {
		if(event.keyCode==13){
			var bat = document.getElementById("keywordInput2").value;
			var trs = document.getElementById("1").value;
		
			// alert("bat : "+bat);
			// alert("trs : "+trs);

			if(bat != trs){
				alert("Previous batch number exist.\n Önceki batch numarası var.");
				document.getElementById("keywordInput2").value = "";
			} else {
				alert("Done.\n tamamlanmış");
				document.getElementById('frm').submit();
			}
		}
	}
</script>
</body>
</html>

