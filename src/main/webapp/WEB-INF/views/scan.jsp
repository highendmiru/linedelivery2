<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false"%>

<html>
<head>
	<title>List</title>
	<script type="text/javascript" language="javascript">
		window.onload = function(){
			 document.getElementById("keywordInput").focus();	
		}
	</script>
</head>
<body>
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<a href="scan">Scan</a>&nbsp;&nbsp;&nbsp;<a href="history">History</a>
			</div>
			<div class='box'>
				<div class="box-header with-border">
					<h3 class="box-title">Line Delivery</h3>
				</div>
				<div class='box-body'>
					Read Job Code</br>
					<input type="hidden" name="searchType" id="searchType" value="job">
					<input type="text" name='keyword' id="keywordInput" onkeydown="searchBtn()" value='${cri.keyword }'>
				</div>
			</div>
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
function searchBtn(){
	if(event.keyCode==13){
		var type1 = "${pageMaker.makeQuery(1)}";
		var type2 = document.getElementById("searchType").value;
		var type3 = document.getElementById("keywordInput").value;

		var strLen = type3.length;

		if(strLen == 4){
			window.location = 'list'+type1+'&searchType='+type2+'&keyword='+type3;
		} else {
			alert("Please scan job code.\n lütfen tara job code.");
		}
	}
}

</script>
</body>
</html>

