<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false"%>

<html>
<head>
	<title>List</title>
	<style type="text/css">
		.highlight {
    		background-color: #fff34d;
    		-moz-border-radius: 5px; /* FF1+ */
    		-webkit-border-radius: 5px; /* Saf3-4 */
    		border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
    		-moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
    		-webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
    		box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
		}

		.highlight {
    		padding:1px 4px;
    		margin:0 -4px;
		}
		table, select {
			width: 210px;
			border-top: 1px solid #444444;
			border-collapse: collapse;
		}
		th, td {
			border-bottom: 1px solid #444444;
			padding: 1px;
			text-align: center;
			font-size: 11px;
		}
	</style>
	<script type="text/javascript" language="javascript">
		window.onload = function(){
			 document.getElementById("keywordInput").focus();
		}
		alert(document.getElementById("searchType").value);
	</script>
</head>
<body>
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<a href="scan">Scan</a>&nbsp;&nbsp;&nbsp;<a href="history">History</a>
			</div>
			<form role="form" action="history" method="post" id='frm'>
			<div class='box'>
				<div class="box-header with-border">
					<h4 class="box-title">Line Delivery History</h4>
				</div>
				<div class='box-body'>
					Read Job Code</br>
					<input type="hidden" name="searchType" id="searchType" value="job">
					<select id="id-lang" name="keyword" onChange="searchBtn()">
						<option value="n"
							<c:out value="${cri.searchType == null?'selected':''}"/>>
							ALL</option>
						<option value="U024" <c:out value="${cri.keyword eq 'U024'?'selected':''}"/>> U024(U024)	ABS/ESP BRKT</option>
						<option value="U032" <c:out value="${cri.keyword eq 'U032'?'selected':''}"/>> U032(U032)	AIR CLEANER</option>
						<option value="U130" <c:out value="${cri.keyword eq 'U130'?'selected':''}"/>> U130(U130)	AIR CON COMPRESSOR</option>
						<option value="U163" <c:out value="${cri.keyword eq 'U163'?'selected':''}"/>> U163(U163)	AIR CON TUBE SUC/LIQ</option>
						<option value="U082" <c:out value="${cri.keyword eq 'U082'?'selected':''}"/>> U082(U082)	AIRBAG CONTROL UNIT</option>
						<option value="U158" <c:out value="${cri.keyword eq 'U158'?'selected':''}"/>> U158(U158)	ANTENNA ASSY ROOF</option>
						<option value="U119" <c:out value="${cri.keyword eq 'U119'?'selected':''}"/>> U119(U119)	ARM & BLADE ASSY LH</option>
						<option value="U120" <c:out value="${cri.keyword eq 'U120'?'selected':''}"/>> U120(U120)	ARM & BLADE ASSY RH</option>
						<option value="U035" <c:out value="${cri.keyword eq 'U035'?'selected':''}"/>> U035(U035)	BATTERY ASSY</option>
						<option value="U044" <c:out value="${cri.keyword eq 'U044'?'selected':''}"/>> U044(U044)	COMPUTER&BRKT ASSY</option>
						<option value="U063" <c:out value="${cri.keyword eq 'U063'?'selected':''}"/>> U063(U063)	COVER ASSY CLUTCH</option>
						<option value="U019" <c:out value="${cri.keyword eq 'U019'?'selected':''}"/>> U019(U019)	COVER WHEEL LH</option>
						<option value="R019" <c:out value="${cri.keyword eq 'R019'?'selected':''}"/>> R019(R019)	COVER WHEEL RH</option>
						<option value="U151" <c:out value="${cri.keyword eq 'U151'?'selected':''}"/>> U151(U151)	COWL TOP COVER</option>
						<option value="U062" <c:out value="${cri.keyword eq 'U062'?'selected':''}"/>> U062(U062)	DISC ASSY CLUTCH</option>
						<option value="U001" <c:out value="${cri.keyword eq 'U001'?'selected':''}"/>> U001(U001)	ENGINE</option>
						<option value="U085" <c:out value="${cri.keyword eq 'U085'?'selected':''}"/>> U085(U085)	ENGINE BRKT</option>
						<option value="U190" <c:out value="${cri.keyword eq 'U190'?'selected':''}"/>> U190(U190)	FEEDER CABLE ANT</option>
						<option value="C002" <c:out value="${cri.keyword eq 'C002'?'selected':''}"/>> C002(C002)	FLOOR CARPET IA</option>
						<option value="U157" <c:out value="${cri.keyword eq 'U157'?'selected':''}"/>> U157(U157)	FUEL NECK</option>
						<option value="U133" <c:out value="${cri.keyword eq 'U133'?'selected':''}"/>> U133(U133)	FUEL TANK</option>
						<option value="U034" <c:out value="${cri.keyword eq 'U034'?'selected':''}"/>> U034(U034)	FUEL TUBE</option>
						<option value="U008" <c:out value="${cri.keyword eq 'U008'?'selected':''}"/>> U008(U008)	GLASS FR DOOR LH</option>
						<option value="R008" <c:out value="${cri.keyword eq 'R008'?'selected':''}"/>> R008(R008)	GLASS FR DOOR RH</option>
						<option value="U015" <c:out value="${cri.keyword eq 'U015'?'selected':''}"/>> U015(U015)	GLASS RR DOOR LH</option>
						<option value="R015" <c:out value="${cri.keyword eq 'R015'?'selected':''}"/>> R015(R015)	GLASS RR DOOR RH</option>
						<option value="U007" <c:out value="${cri.keyword eq 'U007'?'selected':''}"/>> U007(U007)	GLASS RR T/GATE</option>
						<option value="U006" <c:out value="${cri.keyword eq 'U006'?'selected':''}"/>> U006(U006)	GLASS W/S</option>
						<option value="U065" <c:out value="${cri.keyword eq 'U065'?'selected':''}"/>> U065(U065)	ISO PAD</option>
						<option value="U048" <c:out value="${cri.keyword eq 'U048'?'selected':''}"/>> U048(U048)	KEY SET</option>
						<option value="U009" <c:out value="${cri.keyword eq 'U009'?'selected':''}"/>> U009(U009)	LAMP ASSY-HEAD LH</option>
						<option value="R009" <c:out value="${cri.keyword eq 'R009'?'selected':''}"/>> R009(R009)	LAMP ASSY-HEAD RH</option>
						<option value="C029" <c:out value="${cri.keyword eq 'C029'?'selected':''}"/>> C029(C029)	LAMP ASSY-OVERHEAD</option>
						<option value="I132" <c:out value="${cri.keyword eq 'I132'?'selected':''}"/>> I132(I132)	LAMP COMBI I/S RR LH</option>
						<option value="I032" <c:out value="${cri.keyword eq 'I032'?'selected':''}"/>> I032(I032)	LAMP COMBI I/S RR RH</option>
						<option value="U132" <c:out value="${cri.keyword eq 'U132'?'selected':''}"/>> U132(U132)	LAMP COMBI O/S RR LH</option>
						<option value="R132" <c:out value="${cri.keyword eq 'R132'?'selected':''}"/>> R132(R132)	LAMP COMBI O/S RR RH</option>
						<option value="C043" <c:out value="${cri.keyword eq 'C043'?'selected':''}"/>> C043(C043)	LEVER ASSY A/T M</option>
						<option value="C016" <c:out value="${cri.keyword eq 'C016'?'selected':''}"/>> C016(C016)	LUGGAGE FLOOR CVR</option>
						<option value="C014" <c:out value="${cri.keyword eq 'C014'?'selected':''}"/>> C014(C014)	LUGGAGE SIDE LH</option>
						<option value="C015" <c:out value="${cri.keyword eq 'C015'?'selected':''}"/>> C015(C015)	LUGGAGE SIDE RH</option>
						<option value="U023" <c:out value="${cri.keyword eq 'U023'?'selected':''}"/>> U023(U023)	MASTER CYLN/BSTR</option>
						<option value="U012" <c:out value="${cri.keyword eq 'U012'?'selected':''}"/>> U012(U012)	MIRROR I/S</option>
						<option value="U031" <c:out value="${cri.keyword eq 'U031'?'selected':''}"/>> U031(U031)	MIRROR O/S LH</option>
						<option value="U021" <c:out value="${cri.keyword eq 'U021'?'selected':''}"/>> U021(U021)	MIRROR O/S RH</option>
						<option value="B010" <c:out value="${cri.keyword eq 'B010'?'selected':''}"/>> B010(B010)	MUFFLER ASSY (IA)</option>
						<option value="U027" <c:out value="${cri.keyword eq 'U027'?'selected':''}"/>> U027(U027)	MUFFLER CTR (GB)</option>
						<option value="U010" <c:out value="${cri.keyword eq 'U010'?'selected':''}"/>> U010(U010)	MUFFLER FRT (GB)</option>
						<option value="U052" <c:out value="${cri.keyword eq 'U052'?'selected':''}"/>> U052(U052)	O/S HANDLE RR LH</option>
						<option value="R052" <c:out value="${cri.keyword eq 'R052'?'selected':''}"/>> R052(R052)	O/S HANDLE RR RH</option>
						<option value="U050" <c:out value="${cri.keyword eq 'U050'?'selected':''}"/>> U050(U050)	O/S HNDL COVER FR LH</option>
						<option value="U049" <c:out value="${cri.keyword eq 'U049'?'selected':''}"/>> U049(U049)	O/S HNDL COVER FR RH</option>
						<option value="U089" <c:out value="${cri.keyword eq 'U089'?'selected':''}"/>> U089(U089)	PARKING BRAKE LEVER</option>
						<option value="U076" <c:out value="${cri.keyword eq 'U076'?'selected':''}"/>> U076(U076)	PEDAL ACCELERATOR</option>
						<option value="U070" <c:out value="${cri.keyword eq 'U070'?'selected':''}"/>> U070(U070)	PEDAL BREAK</option>
						<option value="U153" <c:out value="${cri.keyword eq 'U153'?'selected':''}"/>> U153(U153)	PEDAL CLUTCH</option>
						<option value="C027" <c:out value="${cri.keyword eq 'C027'?'selected':''}"/>> C027(C027)	PILLAR CTR UPR LH</option>
						<option value="R028" <c:out value="${cri.keyword eq 'R028'?'selected':''}"/>> R028(R028)	PILLAR CTR UPR RH</option>
						<option value="C025" <c:out value="${cri.keyword eq 'C025'?'selected':''}"/>> C025(C025)	PILLAR FR LH</option>
						<option value="C026" <c:out value="${cri.keyword eq 'C026'?'selected':''}"/>> C026(C026)	PILLAR FR RH</option>
						<option value="U147" <c:out value="${cri.keyword eq 'U147'?'selected':''}"/>> U147(U147)	R/PUMP W/WASH</option>
						<option value="U161" <c:out value="${cri.keyword eq 'U161'?'selected':''}"/>> U161(U161)	R/R SPOILER</option>
						<option value="U164" <c:out value="${cri.keyword eq 'U164'?'selected':''}"/>> U164(U164)	REAR SHOCK ABSORBER LH</option>
						<option value="R164" <c:out value="${cri.keyword eq 'R164'?'selected':''}"/>> R164(R164)	REAR SHOCK ABSORBER RH</option>
						<option value="U013" <c:out value="${cri.keyword eq 'U013'?'selected':''}"/>> U013(U013)	SEAT BELT FRT LH</option>
						<option value="U020" <c:out value="${cri.keyword eq 'U020'?'selected':''}"/>> U020(U020)	SEAT BELT FRT RH</option>
						<option value="U129" <c:out value="${cri.keyword eq 'U129'?'selected':''}"/>> U129(U129)	STARTER ASSY</option>
						<option value="U064" <c:out value="${cri.keyword eq 'U064'?'selected':''}"/>> U064(U064)	STEERING WHEEL</option>
						<option value="C012" <c:out value="${cri.keyword eq 'C012'?'selected':''}"/>> C012(C012)	SUNVISOR LH</option>
						<option value="C013" <c:out value="${cri.keyword eq 'C013'?'selected':''}"/>> C013(C013)	SUNVISOR RH</option>
						<option value="C042" <c:out value="${cri.keyword eq 'C042'?'selected':''}"/>> C042(C042)	T.G.S AUTO KNOB</option>
						<option value="U040" <c:out value="${cri.keyword eq 'U040'?'selected':''}"/>> U040(U040)	T/G HND & RR CAM</option>
						<option value="U188" <c:out value="${cri.keyword eq 'U188'?'selected':''}"/>> U188(U188)	T/M BRACKET</option>
						<option value="U126" <c:out value="${cri.keyword eq 'U126'?'selected':''}"/>> U126(U126)	T/M CABLE M/T - A/T</option>
						<option value="U002" <c:out value="${cri.keyword eq 'U002'?'selected':''}"/>> U002(U002)	TRANSMISSION</option>
						<option value="U039" <c:out value="${cri.keyword eq 'U039'?'selected':''}"/>> U039(U039)	V-BELT</option>
						<option value="U168" <c:out value="${cri.keyword eq 'U168'?'selected':''}"/>> U168(U168)	W/ASSY BATTERY</option>
						<option value="U111" <c:out value="${cri.keyword eq 'U111'?'selected':''}"/>> U111(U111)	W/ASSY DOOR DRV</option>
						<option value="U112" <c:out value="${cri.keyword eq 'U112'?'selected':''}"/>> U112(U112)	W/ASSY DOOR PAS</option>
						<option value="U113" <c:out value="${cri.keyword eq 'U113'?'selected':''}"/>> U113(U113)	W/ASSY DOOR RR LH</option>
						<option value="U118" <c:out value="${cri.keyword eq 'U118'?'selected':''}"/>> U118(U118)	W/ASSY DOOR RR RH</option>
						<option value="U137" <c:out value="${cri.keyword eq 'U137'?'selected':''}"/>> U137(U137)	W/ASSY EXT T/GATE</option>
						<option value="U043" <c:out value="${cri.keyword eq 'U043'?'selected':''}"/>> U043(U043)	W/ASSY FLOOR</option>
						<option value="U029" <c:out value="${cri.keyword eq 'U029'?'selected':''}"/>> U029(U029)	W/ASSY FRONT</option>
						<option value="U041" <c:out value="${cri.keyword eq 'U041'?'selected':''}"/>> U041(U041)	W/ASSY T/GATE</option>
						<option value="U068" <c:out value="${cri.keyword eq 'U068'?'selected':''}"/>> U068(U068)	WIPER MOTOR W/S</option>
						<option value="M001" <c:out value="${cri.keyword eq 'M001'?'selected':''}"/>> M001(M001)	WIRE HARNESS</option>
					</select> 
					</form>
				</div>
				</br>
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered">
						<tr>
							<th>BAT</th>
							<th style="background-color:#dbdbdb;">JOB</th>
							<th>DATE</th>
						</tr>
						<c:forEach items="${history}" var="LineDVO" varStatus="status">
							<tr>
								<td><input type="hidden" id="${status.count}" value="${LineDVO.bat}">${LineDVO.bat}</td>
								<td style="background-color:#dbdbdb;">${LineDVO.job}</td>
								<td><fmt:formatDate value="${LineDVO.regdate}" pattern="yyyy.MM.dd HH:mm:ss" /></td>  								
							</tr>
						</c:forEach>

					</table>
				</div>
				<div class="box-footer">

					<div class="text-center">
						<table class="pagination">
							<tr>
								<td>
									<c:if test="${pageMaker.prev}">
										<a href="history${pageMaker.makeSearch(pageMaker.startPage - 1) }">&laquo;</a>
									</c:if>
								</td>
								<c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }" var="idx">
									<td	<c:out value="${pageMaker.cri.page == idx?'class =active':''}"/>>
										<a href="history${pageMaker.makeSearch(idx)}">${idx}</a>
									</td>
								</c:forEach>
								<c:if test="${pageMaker.next && pageMaker.endPage > 0}">
									<td><a href="history${pageMaker.makeSearch(pageMaker.endPage +1) }">&raquo;</a></td>
								</c:if>
							</tr>
						</table>
					</div>
			</div>
		</div>
		<!--/.col (left) -->
		</div>
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
function searchBtn(){   
	var type1 = "${pageMaker.makeQuery(1)}";
	var type2 = document.getElementById("searchType").value;
	// var type3 = document.getElementById("keywordInput").value;
    var langSelect = document.getElementById("id-lang");
    
    // select element에서 선택된 option의 value가 저장된다.
    var type3 = langSelect.options[langSelect.selectedIndex].value;
	var strLen = type3.length;
	
	if(strLen == 4){
		window.location = 'history'+type1+'&searchType='+type2+'&keyword='+type3;
	} else {
		alert("Please scan job code.\n lütfen tara job code.");
	}
}

</script>
</body>
</html>

