package org.gtr.persistence;

import java.util.List;

import org.gtr.domain.LineDVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface LineDDAO {

  public void create(LineDVO lo) throws Exception;

  public LineDVO read(String bat) throws Exception;

//  public void update(LineDVO lo) throws Exception;
  public void update(String bat) throws Exception;

  public void delete(String bat) throws Exception;

  public List<LineDVO> listAll() throws Exception;

  public List<LineDVO> listPage(int page) throws Exception;
  
  public List<LineDVO> hlistPage(int page) throws Exception;

  public List<LineDVO> listCriteria(Criteria cri) throws Exception;

  public int countPaging(Criteria cri) throws Exception;
  
  //use for dynamic sql
  
  public List<LineDVO> listSearch(SearchCriteria cri)throws Exception;
  
  public List<LineDVO> listSearch2(SearchCriteria cri)throws Exception;
  
  public int listSearchCount(SearchCriteria cri)throws Exception;
  
  public void addAttach(String fullName)throws Exception;
  
  public List<String> getAttach(String bat)throws Exception;  
   
  public void deleteAttach(String bat)throws Exception;
  
  public void replaceAttach(String fullName, String bat)throws Exception;

}
