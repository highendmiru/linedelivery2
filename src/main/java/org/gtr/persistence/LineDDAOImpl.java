package org.gtr.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.gtr.domain.LineDVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

@Repository
public class LineDDAOImpl implements LineDDAO {

  @Inject
  private SqlSession session;

  private static String namespace = "org.gtr.mapper.LineDMapper";

  @Override
  public void create(LineDVO lo) throws Exception {
    session.insert(namespace + ".create", lo);
  }

  @Override
  public LineDVO read(String bat) throws Exception {
    return session.selectOne(namespace + ".read", bat);
  }

  @Override
  //public void update(LineDVO lo) throws Exception {
  //  session.update(namespace + ".update", lo);
  //}

  public void update(String bat) throws Exception {
	    session.update(namespace + ".update", bat);
	  }
  
  @Override
  public void delete(String bat) throws Exception {
    session.delete(namespace + ".delete", bat);
  }

  @Override
  public List<LineDVO> listAll() throws Exception {
    return session.selectList(namespace + ".listAll");
  }

  @Override
  public List<LineDVO> listPage(int page) throws Exception {

    if (page <= 0) {
      page = 1;
    }

    page = (page - 1) * 10;

    return session.selectList(namespace + ".listPage", page);
  }

  public List<LineDVO> hlistPage(int page) throws Exception {

	    if (page <= 0) {
	      page = 1;
	    }

	    page = (page - 1) * 10;

	    return session.selectList(namespace + ".listPage", page);
  }
  
  @Override
  public List<LineDVO> listCriteria(Criteria cri) throws Exception {

    return session.selectList(namespace + ".listCriteria", cri);
  }

  @Override
  public int countPaging(Criteria cri) throws Exception {

    return session.selectOne(namespace + ".countPaging", cri);
  }

  @Override
  public List<LineDVO> listSearch(SearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".listSearch", cri);
  }
  
  @Override
  public List<LineDVO> listSearch2(SearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".listSearch2", cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return session.selectOne(namespace + ".listSearchCount", cri);
  }
  @Override
  public void addAttach(String fullName) throws Exception {
    
    session.insert(namespace+".addAttach", fullName);
    
  }
  
  @Override
  public List<String> getAttach(String bat) throws Exception {
    
    return session.selectList(namespace +".getAttach", bat);
  }
 

  @Override
  public void deleteAttach(String bat) throws Exception {

    session.delete(namespace+".deleteAttach", bat);
    
  }

  @Override
  public void replaceAttach(String fullName, String bat) throws Exception {
    
    Map<String, Object> paramMap = new HashMap<String, Object>();
    
    paramMap.put("bat", bat);
    paramMap.put("fullName", fullName);
    
    session.insert(namespace+".replaceAttach", paramMap);
    
  }

}
