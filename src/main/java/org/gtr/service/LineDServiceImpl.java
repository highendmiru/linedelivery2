package org.gtr.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.gtr.domain.LineDVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.persistence.LineDDAO;

@Service
public class LineDServiceImpl implements LineDService {

  @Inject
  private LineDDAO dao;

  @Override
  public void regist(LineDVO lined) throws Exception {
    dao.create(lined);
    
    String[] files = lined.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
    	dao.addAttach(fileName);
    }
  }

  @Override
  public LineDVO read(String bat) throws Exception {
    return dao.read(bat);
  }

  @Override
/*  public void modify(LineDVO lined) throws Exception {
    dao.update(lined);
    
    String bat = lined.getBat();
    
    dao.deleteAttach(bat);
    
    String[] files = lined.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
      dao.replaceAttach(fileName, bat);
    }
  } */
  
  public void modify(String bat) throws Exception {
	    dao.update(bat);
	  }

  @Override
  public void remove(String bat) throws Exception {
    dao.delete(bat);
  }

  @Override
  public List<LineDVO> listAll() throws Exception {
    return dao.listAll();
  }

  @Override
  public List<LineDVO> listCriteria(Criteria cri) throws Exception {

    return dao.listCriteria(cri);
  }

  @Override
  public int listCountCriteria(Criteria cri) throws Exception {

    return dao.countPaging(cri);
  }

  @Override
  public List<LineDVO> listSearchCriteria(SearchCriteria cri) throws Exception {

    return dao.listSearch(cri);
  }

  @Override
  public List<LineDVO> listSearchCriteria2(SearchCriteria cri) throws Exception {

    return dao.listSearch2(cri);
  }
  
  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return dao.listSearchCount(cri);
  }
  
  @Override
  public List<String> getAttach(String bat) throws Exception {
    
    return dao.getAttach(bat);
  }   

}
