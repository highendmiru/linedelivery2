package org.gtr.service;

import java.util.List;

import org.gtr.domain.LineDVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface LineDService {

  public void regist(LineDVO lined) throws Exception;

  public LineDVO read(String bat) throws Exception;

  //public void modify(LineDVO lined) throws Exception;
  public void modify(String bat) throws Exception;

  public void remove(String bat) throws Exception;

  public List<LineDVO> listAll() throws Exception;

  public List<LineDVO> listCriteria(Criteria cri) throws Exception;

  public int listCountCriteria(Criteria cri) throws Exception;

  public List<LineDVO> listSearchCriteria(SearchCriteria cri) 
      throws Exception;
  
  public List<LineDVO> listSearchCriteria2(SearchCriteria cri) 
	      throws Exception;


  public int listSearchCount(SearchCriteria cri) throws Exception;
  
  public List<String> getAttach(String bat)throws Exception;
}
