package org.gtr.domain;

import java.util.Date;
import java.util.Arrays;

public class LineDVO {

	private String bat;
	private String job;
	private Date regdate;
	private String chkd;
	private String odt;
	private int rownum;

	private String[] files;
	
	public int getRownum() {
		return rownum;
	}
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}
	public String getBat() {
		return bat;
	}
	public void setBat(String bat) {
		this.bat = bat;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public String getChkd() {
		return chkd;
	}
	public void setChkd(String chkd) {
		this.chkd = chkd;
	}
	public String getOdt() {
		return odt;
	}
	public void setOdt(String odt) {
		this.odt = odt;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] flies) {
		this.files = files;
	}
	@Override
	public String toString() {
		return "LineDVO [rownum="+ rownum +",bat=" + bat + ", job=" + job
		+ ", getRegdate()=" + getRegdate()
		+ ", chkd=" + chkd + ", odt=" + odt + ", files=" + Arrays.toString(files) + "]";
	}
}
